angular.module('starter.services', [])

.factory('Activities', function() {
  // Might use a resource here that returns a JSON array

  var activities = [
    {
      id: 0,
      name: 'Bicycling',
      face: 'img/act/bicycling.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'bicycling, mountain, uphill, vigorous',
           metValue: 14,
           metCategory: 2
         },
         {
           id:1,
           name: 'bicycling, mountain, competitive, racing',
           metValue: 16,
           metCategory: 2
         },
         {
           id:2,
           name: 'bicycling, BMX',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:3,
           name: 'bicycling, mountain, general',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:4,
           name: 'bicycling, <10 mph, leisure, to work or for pleasure (Taylor Code 115)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:5,
           name: 'bicycling, to/from work, self selected pace',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:6,
           name: 'bicycling, on dirt or farm road, moderate pace',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:7,
           name: 'bicycling, general',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:8,
           name: 'bicycling, leisure, 5.5 mph',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:9,
           name: 'bicycling, leisure, 9.4 mph',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:10,
           name: 'bicycling, 10-11.9 mph, leisure, slow, light effort',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:11,
           name: 'bicycling, 12-13.9 mph, leisure, moderate effort',
           metValue: 8,
           metCategory: 2
         },
         {
           id:12,
           name: 'bicycling, 14-15.9 mph, racing or leisure, fast, vigorous effort',
           metValue: 10,
           metCategory: 2
         },
         {
           id:13,
           name: 'bicycling, 16-19 mph, racing/not drafting or > 19 mph drafting, very fast, racing general',
           metValue: 12,
           metCategory: 2
         },
         {
           id:14,
           name: 'bicycling, > 20 mph, racing, not drafting',
           metValue: 15.8,
           metCategory: 2
         },
         {
           id:15,
           name: 'bicycling, 12 mph, seated, hands on brake hoods or bar drops, 80 rpm',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:16,
           name: 'bicycling, 12 mph, standing, hands on brake hoods, 60 rpm',
           metValue: 9,
           metCategory: 2
         },
         {
           id:17,
           name: 'unicycling',
           metValue: 5,
           metCategory: 1
         }
        ]
      },
    {
      id: 1,
      name: 'Conditioning Exercise',
      face: 'img/act/conditioning_exercise.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'activity promoting video game (e.g., Wii Fit), light effort (e.g., balance, yoga)',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:1,
           name: 'activity promoting video game (e.g., Wii Fit), moderate effort (e.g., aerobic, resistance)',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:2,
           name: 'activity promoting video/arcade game (e.g., Exergaming, Dance Dance Revolution), vigorous effort',
           metValue: 7.2,
           metCategory: 2
         },
         {
           id:3,
           name: 'army type obstacle course exercise, boot camp training program',
           metValue: 5,
           metCategory: 1
         },
         {
           id:4,
           name: 'bicycling, stationary, general',
           metValue: 7,
           metCategory: 2
         },
         {
           id:5,
           name: 'bicycling, stationary, 30-50 watts, very light to light effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:6,
           name: 'bicycling, stationary, 90-100 watts, moderate to vigorous effort',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:7,
           name: 'bicycling, stationary, 101-160 watts, vigorous effort',
           metValue: 8.8,
           metCategory: 2
         },
         {
           id:8,
           name: 'bicycling, stationary, 161-200 watts, vigorous effort',
           metValue: 11,
           metCategory: 2
         },
         {
           id:9,
           name: 'bicycling, stationary, 201-270 watts, very vigorous effort',
           metValue: 14,
           metCategory: 2
         },
         {
           id:10,
           name: 'bicycling, stationary, 51-89 watts, light-to-moderate effort',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:11,
           name: 'bicycling, stationary, RPM/Spin bike class',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:12,
           name: 'calisthenics (e.g., push ups, sit ups, pull-ups, jumping jacks), vigorous effort',
           metValue: 8,
           metCategory: 2
         },
         {
           id:13,
           name: 'calisthenics (e.g., push ups, sit ups, pull-ups, lunges), moderate effort',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:14,
           name: 'calisthenics (e.g., situps, abdominal crunches), light effort',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:15,
           name: 'calisthenics, light or moderate effort, general (e.g., back exercises), going up & down from floor (Taylor Code 150)',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:16,
           name: 'circuit training, moderate effort',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:17,
           name: 'circuit training, including kettlebells, some aerobic movement with minimal rest, general, vigorous intensity',
           metValue: 8,
           metCategory: 2
         },
         {
           id:18,
           name: 'CurvesTM exercise routines in women',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:19,
           name: 'Elliptical trainer, moderate effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:20,
           name: 'resistance training (weight lifting, free weight, nautilus or universal), power lifting or body building, vigorous effort (Taylor Code 210)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:21,
           name: 'resistance (weight) training, squats , slow or explosive effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:22,
           name: 'resistance (weight) training, multiple exercises, 8-15 repetitions at varied resistance',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:23,
           name: 'health club exercise, general (Taylor Code 160)',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:24,
           name: 'health club exercise classes, general, gym/weight training combined in one visit',
           metValue: 5,
           metCategory: 1
         },
         {
           id:25,
           name: 'health club exercise, conditioning classes',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:26,
           name: 'home exercise, general',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:27,
           name: 'stair-treadmill ergometer, general',
           metValue: 9,
           metCategory: 2
         },
         {
           id:28,
           name: 'rope skipping, general',
           metValue: 12.3,
           metCategory: 2
         },
         {
           id:29,
           name: 'rowing, stationary ergometer, general, vigorous effort',
           metValue: 6,
           metCategory: 2
         },
         {
           id:30,
           name: 'rowing, stationary, general, moderate effort',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:31,
           name: 'rowing, stationary, 100 watts, moderate effort',
           metValue: 7,
           metCategory: 2
         },
         {
           id:32,
           name: 'rowing, stationary, 150 watts, vigorous effort',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:33,
           name: 'rowing, stationary, 200 watts, very vigorous effort',
           metValue: 12,
           metCategory: 2
         },
         {
           id:34,
           name: 'ski machine, general',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:35,
           name: 'slide board exercise, general',
           metValue: 11,
           metCategory: 2
         },
         {
           id:36,
           name: 'slimnastics, jazzercise',
           metValue: 6,
           metCategory: 2
         },
         {
           id:37,
           name: 'stretching, mild',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:38,
           name: 'pilates, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:39,
           name: 'teaching exercise class (e.g., aerobic, water)',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:40,
           name: 'therapeutic exercise ball, Fitball exercise',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:41,
           name: 'upper body exercise, arm ergometer',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:42,
           name: 'upper body exercise, stationary bicycle - Airdyne (arms only) 40 rpm, moderate',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:43,
           name: 'water aerobics, water calisthenics, water exercise',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:44,
           name: 'whirlpool, sitting',
           metValue: 1.3,
           metCategory: 0
         },
         {
           id:45,
           name: 'video exercise workouts, TV conditioning programs (e.g., yoga, stretching), light effort',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:46,
           name: 'video exercise workouts, TV conditioning programs (e.g., cardio-resistance), moderate effort',
           metValue: 4,
           metCategory: 1
         },
         {
           id:47,
           name: 'video exercise workouts, TV conditioning programs (e.g., cardio-resistance), vigorous effort',
           metValue: 6,
           metCategory: 2
         },
         {
           id:48,
           name: 'yoga, Hatha',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:49,
           name: 'yoga, Power',
           metValue: 4,
           metCategory: 1
         },
         {
           id:50,
           name: 'yoga, Nadisodhana',
           metValue: 2,
           metCategory: 0
         },
         {
           id:51,
           name: 'yoga, Surya Namaskar',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:52,
           name: 'native New Zealander physical activities (e.g., Haka Powhiri, Moteatea, Waita Tira, Whakawatea, etc.), general, moderate effort',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:53,
           name: 'native New Zealander physical activities (e.g., Haka, Taiahab), general, vigorous effort',
           metValue: 6.8,
           metCategory: 2
         }
        ]
      },
    {
      id: 2,
      name: 'Dancing',
      face: 'img/act/dancing.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'ballet, modern, or jazz, general, rehearsal or class',
           metValue: 5,
           metCategory: 1
         },
         {
           id:1,
           name: 'ballet, modern, or jazz, performance, vigorous effort',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:2,
           name: 'tap',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:3,
           name: 'aerobic, general',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:4,
           name: 'aerobic, step, with 6 - 8 inch step',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:5,
           name: 'aerobic, step, with 10 - 12 inch step',
           metValue: 9.5,
           metCategory: 2
         },
         {
           id:6,
           name: 'aerobic, step, with 4-inch step',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:7,
           name: 'bench step class, general',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:8,
           name: 'aerobic, low impact',
           metValue: 5,
           metCategory: 1
         },
         {
           id:9,
           name: 'aerobic, high impact',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:10,
           name: 'aerobic dance wearing 10-15 lb weights',
           metValue: 10,
           metCategory: 2
         },
         {
           id:11,
           name: 'ethnic or cultural dancing (e.g., Greek, Middle Eastern, hula, salsa, merengue, bamba y plena, flamenco, belly, and swing)',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:12,
           name: 'ballroom, fast (Taylor Code 125)',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:13,
           name: 'general dancing (e.g., disco, folk, Irish step dancing, line dancing, polka, contra, country)',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:14,
           name: 'ballroom dancing, competitive, general',
           metValue: 11.3,
           metCategory: 2
         },
         {
           id:15,
           name: 'ballroom, slow (e.g., waltz, foxtrot, slow dancing, samba, tango, 19th century dance, mambo, cha cha)',
           metValue: 3,
           metCategory: 1
         },
         {
           id:16,
           name: 'Anishinaabe Jingle Dancing',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:17,
           name: 'Caribbean dance (Abakua, Beguine, Bellair, Bongo, Brukins, Caribbean Quadrills, Dinki Mini, Gere, Gumbay, Ibo, Jonkonnu, Kumina, Oreisha, Jambu)',
           metValue: 3.5,
           metCategory: 1
         }
        ]
      },
    {
      id: 3,
      name: 'Fishing and Hunting',
      face: 'img/act/fishing_and_hunting.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'fishing, general',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:1,
           name: 'fishing, crab fishing',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:2,
           name: 'fishing, catching fish with hands',
           metValue: 4,
           metCategory: 1
         },
         {
           id:3,
           name: 'fishing related, digging worms, with shovel',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:4,
           name: 'fishing from river bank and walking',
           metValue: 4,
           metCategory: 1
         },
         {
           id:5,
           name: 'fishing from boat or canoe, sitting',
           metValue: 2,
           metCategory: 0
         },
         {
           id:6,
           name: 'fishing from river bank, standing (Taylor Code 660)',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:7,
           name: 'fishing in stream, in waders (Taylor Code 670)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:8,
           name: 'fishing, ice, sitting',
           metValue: 2,
           metCategory: 0
         },
         {
           id:9,
           name: 'fishing, jog or line, standing, general',
           metValue: 1.8,
           metCategory: 0
         },
         {
           id:10,
           name: 'fishing, dip net, setting net and retrieving fish, general',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:11,
           name: 'fishing, set net, setting net and retrieving fish, general',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:12,
           name: 'fishing, fishing wheel, setting net and retrieving fish, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:13,
           name: 'fishing with a spear, standing',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:14,
           name: 'hunting, bow and arrow, or crossbow',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:15,
           name: 'hunting, deer, elk, large game (Taylor Code 170)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:16,
           name: 'hunting large game, dragging carcass',
           metValue: 11.3,
           metCategory: 2
         },
         {
           id:17,
           name: 'hunting large marine animals',
           metValue: 4,
           metCategory: 1
         },
         {
           id:18,
           name: 'hunting large game, from a hunting stand, limited walking',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:19,
           name: 'hunting large game from a car, plane, or boat',
           metValue: 2,
           metCategory: 0
         },
         {
           id:20,
           name: 'hunting, duck, wading',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:21,
           name: 'hunting, flying fox, squirrel',
           metValue: 3,
           metCategory: 1
         },
         {
           id:22,
           name: 'hunting, general',
           metValue: 5,
           metCategory: 1
         },
         {
           id:23,
           name: 'hunting, pheasants or grouse (Taylor Code 680)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:24,
           name: 'hunting, birds',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:25,
           name: 'hunting, rabbit, squirrel, prairie chick, raccoon, small game (Taylor Code 690)',
           metValue: 5,
           metCategory: 1
         },
         {
           id:26,
           name: 'hunting, pigs, wild',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:27,
           name: 'trapping game, general',
           metValue: 2,
           metCategory: 0
         },
         {
           id:28,
           name: 'hunting, hiking with hunting gear',
           metValue: 9.5,
           metCategory: 2
         },
         {
           id:29,
           name: 'pistol shooting or trap shooting, standing',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:30,
           name: 'rifle exercises, shooting, lying down',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:31,
           name: 'rifle exercises, shooting, kneeling or standing',
           metValue: 2.5,
           metCategory: 0
         }
        ]
      },
    {
      id: 4,
      name: 'Lawn and Garden',
      face: 'img/act/lawn_and_garden.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'carrying, loading or stacking wood, loading/unloading or carrying lumber, light-to-moderate effort',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:1,
           name: 'carrying, loading or stacking wood, loading/unloading or carrying lumber',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:2,
           name: 'chopping wood, splitting logs, moderate effort',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:3,
           name: 'chopping wood, splitting logs, vigorous effort',
           metValue: 6.3,
           metCategory: 2
         },
         {
           id:4,
           name: 'clearing light brush, thinning garden, moderate effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:5,
           name: 'clearing brush/land, undergrowth, or ground, hauling branches, wheelbarrow chores, vigorous effort',
           metValue: 6.3,
           metCategory: 2
         },
         {
           id:6,
           name: 'digging sandbox, shoveling sand',
           metValue: 5,
           metCategory: 1
         },
         {
           id:7,
           name: 'digging, spading, filling garden, composting, light-to-moderate effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:8,
           name: 'digging, spading, filling garden, compositing, (Taylor Code 590)',
           metValue: 5,
           metCategory: 1
         },
         {
           id:9,
           name: 'digging, spading, filling garden, composting, vigorous effort',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:10,
           name: 'driving tractor',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:11,
           name: 'felling trees, large size',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:12,
           name: 'felling trees, small-medium size',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:13,
           name: 'gardening with heavy power tools, tilling a garden, chain saw',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:14,
           name: 'gardening, using containers, older adults > 60 years',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:15,
           name: 'irrigation channels, opening and closing ports',
           metValue: 4,
           metCategory: 1
         },
         {
           id:16,
           name: 'laying crushed rock',
           metValue: 6.3,
           metCategory: 2
         },
         {
           id:17,
           name: 'laying sod',
           metValue: 5,
           metCategory: 1
         },
         {
           id:18,
           name: 'mowing lawn, general',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:19,
           name: 'mowing lawn, riding mower (Taylor Code 550)',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:20,
           name: 'mowing lawn, walk, hand mower (Taylor Code 570)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:21,
           name: 'mowing lawn, walk, power mower, moderate or vigorous effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:22,
           name: 'mowing lawn, power mower, light or moderate effort (Taylor Code 590)',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:23,
           name: 'operating snow blower, walking',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:24,
           name: 'planting, potting, transplanting seedlings or plants, light effort',
           metValue: 2,
           metCategory: 0
         },
         {
           id:25,
           name: 'planting seedlings, shrub, stooping, moderate effort',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:26,
           name: 'planting crops or garden, stooping, moderate effort',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:27,
           name: 'planting trees',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:28,
           name: 'raking lawn or leaves, moderate effort',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:29,
           name: 'raking lawn (Taylor Code 600)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:30,
           name: 'raking roof with snow rake',
           metValue: 4,
           metCategory: 1
         },
         {
           id:31,
           name: 'riding snow blower',
           metValue: 3,
           metCategory: 1
         },
         {
           id:32,
           name: 'sacking grass, leaves',
           metValue: 4,
           metCategory: 1
         },
         {
           id:33,
           name: 'shoveling dirt or mud',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:34,
           name: 'shoveling snow, by hand, moderate effort',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:35,
           name: 'shovelling snow, by hand (Taylor Code 610)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:36,
           name: 'shoveling snow, by hand, vigorous effort',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:37,
           name: 'trimming shrubs or trees, manual cutter',
           metValue: 4,
           metCategory: 1
         },
         {
           id:38,
           name: 'trimming shrubs or trees, power cutter, using leaf blower, edge, moderate effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:39,
           name: 'walking, applying fertilizer or seeding a lawn, push applicator',
           metValue: 3,
           metCategory: 1
         },
         {
           id:40,
           name: 'watering lawn or garden, standing or walking',
           metValue: 1.5,
           metCategory: 0
         },
         {
           id:41,
           name: 'weeding, cultivating garden, light-to-moderate effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:42,
           name: 'weeding, cultivating garden (Taylor Code 580)',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:43,
           name: 'weeding, cultivating garden, using a hoe, moderate-to-vigorous effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:44,
           name: 'gardening, general, moderate effort',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:45,
           name: 'picking fruit off trees, picking fruits/vegetables, moderate effort',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:46,
           name: 'picking fruit off trees, gleaning fruits, picking fruits/vegetables, climbing ladder to pick fruit, vigorous effort',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:47,
           name: 'implied walking/standing - picking up yard, light, picking flowers or vegetables',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:48,
           name: 'walking, gathering gardening tools',
           metValue: 3,
           metCategory: 1
         },
         {
           id:49,
           name: 'wheelbarrow, pushing garden cart or wheelbarrow',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:50,
           name: 'yard work, general, light effort',
           metValue: 3,
           metCategory: 1
         },
         {
           id:51,
           name: 'yard work, general, moderate effort',
           metValue: 4,
           metCategory: 1
         },
         {
           id:52,
           name: 'yard work, general, vigorous effort',
           metValue: 6,
           metCategory: 2
         }
        ]
      },
    {
      id: 5,
      name: 'Running',
      face: 'img/act/running.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'jog/walk combination (jogging component of less than 10 minutes) (Taylor Code 180)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:1,
           name: 'jogging, general',
           metValue: 7,
           metCategory: 2
         },
         {
           id:2,
           name: 'jogging, in place',
           metValue: 8,
           metCategory: 2
         },
         {
           id:3,
           name: 'jogging, on a mini-tramp',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:4,
           name: 'Running, 4 mph (13 min/mile)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:5,
           name: 'running, 5 mph (12 min/mile)',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:6,
           name: 'running, 5.2 mph (11.5 min/mile)',
           metValue: 9,
           metCategory: 2
         },
         {
           id:7,
           name: 'running, 6 mph (10 min/mile)',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:8,
           name: 'running, 6.7 mph (9 min/mile)',
           metValue: 10.5,
           metCategory: 2
         },
         {
           id:9,
           name: 'running, 7 mph (8.5 min/mile)',
           metValue: 11,
           metCategory: 2
         },
         {
           id:10,
           name: 'running, 7.5 mph (8 min/mile)',
           metValue: 11.5,
           metCategory: 2
         },
         {
           id:11,
           name: 'running, 8 mph (7.5 min/mile)',
           metValue: 11.8,
           metCategory: 2
         },
         {
           id:12,
           name: 'running, 8.6 mph (7 min/mile)',
           metValue: 12.3,
           metCategory: 2
         },
         {
           id:13,
           name: 'running, 9 mph (6.5 min/mile)',
           metValue: 12.8,
           metCategory: 2
         },
         {
           id:14,
           name: 'running, 10 mph (6 min/mile)',
           metValue: 14.5,
           metCategory: 2
         },
         {
           id:15,
           name: 'running, 11 mph (5.5 min/mile)',
           metValue: 16,
           metCategory: 2
         },
         {
           id:16,
           name: 'running, 12 mph (5 min/mile)',
           metValue: 19,
           metCategory: 2
         },
         {
           id:17,
           name: 'running, 13 mph (4.6 min/mile)',
           metValue: 19.8,
           metCategory: 2
         },
         {
           id:18,
           name: 'running, 14 mph (4.3 min/mile)',
           metValue: 23,
           metCategory: 2
         },
         {
           id:19,
           name: 'running, cross country',
           metValue: 9,
           metCategory: 2
         },
         {
           id:20,
           name: 'running, (Taylor code 200)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:21,
           name: 'running, stairs, up',
           metValue: 15,
           metCategory: 2
         },
         {
           id:22,
           name: 'running, on a track, team practice',
           metValue: 10,
           metCategory: 2
         },
         {
           id:23,
           name: 'running, training, pushing a wheelchair or baby carrier',
           metValue: 8,
           metCategory: 2
         },
         {
           id:24,
           name: 'running, marathon',
           metValue: 13.3,
           metCategory: 2
         }
        ]
      },
    {
      id: 6,
      name: 'Sports',
      face: 'img/act/sports.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'Alaska Native Games, Eskimo Olympics, general',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:1,
           name: 'archery, non-hunting',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:2,
           name: 'badminton, competitive (Taylor Code 450)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:3,
           name: 'badminton, social singles and doubles, general',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:4,
           name: 'basketball, game (Taylor Code 490)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:5,
           name: 'basketball, non-game, general (Taylor Code 480)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:6,
           name: 'basketball, general',
           metValue: 6.5,
           metCategory: 2
         },
         {
           id:7,
           name: 'basketball, officiating (Taylor Code 500)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:8,
           name: 'basketball, shooting baskets',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:9,
           name: 'basketball, drills, practice',
           metValue: 9.3,
           metCategory: 2
         },
         {
           id:10,
           name: 'basketball, wheelchair',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:11,
           name: 'billiards',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:12,
           name: 'bowling (Taylor Code 390)',
           metValue: 3,
           metCategory: 1
         },
         {
           id:13,
           name: 'bowling, indoor, bowling alley',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:14,
           name: 'boxing, in ring, general',
           metValue: 12.8,
           metCategory: 2
         },
         {
           id:15,
           name: 'boxing, punching bag',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:16,
           name: 'boxing, sparring',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:17,
           name: 'broomball',
           metValue: 7,
           metCategory: 2
         },
         {
           id:18,
           name: 'childrenÆs games, adults playing (e.g., hopscotch, 4-square, dodgeball, playground apparatus, t-ball, tetherball, marbles, arcade games), moderate effort',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:19,
           name: 'cheerleading, gymnastic moves, competitive',
           metValue: 6,
           metCategory: 2
         },
         {
           id:20,
           name: 'coaching, football, soccer, basketball, baseball, swimming, etc.',
           metValue: 4,
           metCategory: 1
         },
         {
           id:21,
           name: 'coaching, actively playing sport with players',
           metValue: 8,
           metCategory: 2
         },
         {
           id:22,
           name: 'cricket, batting, bowling, fielding',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:23,
           name: 'croquet',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:24,
           name: 'curling',
           metValue: 4,
           metCategory: 1
         },
         {
           id:25,
           name: 'darts, wall or lawn',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:26,
           name: 'drag racing, pushing or driving a car',
           metValue: 6,
           metCategory: 2
         },
         {
           id:27,
           name: 'auto racing, open wheel',
           metValue: 8.5,
           metCategory: 2
         },
         {
           id:28,
           name: 'fencing',
           metValue: 6,
           metCategory: 2
         },
         {
           id:29,
           name: 'football, competitive',
           metValue: 8,
           metCategory: 2
         },
         {
           id:30,
           name: 'football, touch, flag, general (Taylor Code 510)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:31,
           name: 'football, touch, flag, light effort',
           metValue: 4,
           metCategory: 1
         },
         {
           id:32,
           name: 'football or baseball, playing catch',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:33,
           name: 'frisbee playing, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:34,
           name: 'frisbee, ultimate',
           metValue: 8,
           metCategory: 2
         },
         {
           id:35,
           name: 'golf, general',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:36,
           name: 'golf, walking, carrying clubs',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:37,
           name: 'golf, miniature, driving range',
           metValue: 3,
           metCategory: 1
         },
         {
           id:38,
           name: 'golf, walking, pulling clubs',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:39,
           name: 'golf, using power cart (Taylor Code 070)',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:40,
           name: 'gymnastics, general',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:41,
           name: 'hacky sack',
           metValue: 4,
           metCategory: 1
         },
         {
           id:42,
           name: 'handball, general (Taylor Code 520)',
           metValue: 12,
           metCategory: 2
         },
         {
           id:43,
           name: 'handball, team',
           metValue: 8,
           metCategory: 2
         },
         {
           id:44,
           name: 'high ropes course, multiple elements',
           metValue: 4,
           metCategory: 1
         },
         {
           id:45,
           name: 'hang gliding',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:46,
           name: 'hockey, field',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:47,
           name: 'hockey, ice, general',
           metValue: 8,
           metCategory: 2
         },
         {
           id:48,
           name: 'hockey, ice, competitive',
           metValue: 10,
           metCategory: 2
         },
         {
           id:49,
           name: 'horseback riding, general',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:50,
           name: 'horse chores, feeding, watering, cleaning stalls, implied walking and lifting loads',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:51,
           name: 'saddling, cleaning, grooming, harnessing and unharnessing horse',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:52,
           name: 'horseback riding, trotting',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:53,
           name: 'horseback riding, canter or gallop',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:54,
           name: 'horseback riding,walking',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:55,
           name: 'horseback riding, jumping',
           metValue: 9,
           metCategory: 2
         },
         {
           id:56,
           name: 'horse cart, driving, standing or sitting',
           metValue: 1.8,
           metCategory: 0
         },
         {
           id:57,
           name: 'horseshoe pitching, quoits',
           metValue: 3,
           metCategory: 1
         },
         {
           id:58,
           name: 'jai alai',
           metValue: 12,
           metCategory: 2
         },
         {
           id:59,
           name: 'martial arts, different types, slower pace, novice performers, practice',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:60,
           name: 'martial arts, different types, moderate pace (e.g., judo, jujitsu, karate, kick boxing, tae kwan do, tai-bo, Muay Thai boxing)',
           metValue: 10.3,
           metCategory: 2
         },
         {
           id:61,
           name: 'juggling',
           metValue: 4,
           metCategory: 1
         },
         {
           id:62,
           name: 'kickball',
           metValue: 7,
           metCategory: 2
         },
         {
           id:63,
           name: 'lacrosse',
           metValue: 8,
           metCategory: 2
         },
         {
           id:64,
           name: 'lawn bowling, bocce ball, outdoor',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:65,
           name: 'moto-cross, off-road motor sports, all-terrain vehicle, general',
           metValue: 4,
           metCategory: 1
         },
         {
           id:66,
           name: 'orienteering',
           metValue: 9,
           metCategory: 2
         },
         {
           id:67,
           name: 'paddleball, competitive',
           metValue: 10,
           metCategory: 2
         },
         {
           id:68,
           name: 'paddleball, casual, general (Taylor Code 460)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:69,
           name: 'polo, on horseback',
           metValue: 8,
           metCategory: 2
         },
         {
           id:70,
           name: 'racquetball, competitive',
           metValue: 10,
           metCategory: 2
         },
         {
           id:71,
           name: 'racquetball, general (Taylor Code 470)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:72,
           name: 'rock or mountain climbing (Taylor Code 470) (Formerly code = 17120)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:73,
           name: 'rock climbing, ascending rock, high difficulty',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:74,
           name: 'rock climbing, ascending or traversing rock, low-to-moderate difficulty',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:75,
           name: 'rock climbing, rappelling',
           metValue: 5,
           metCategory: 1
         },
         {
           id:76,
           name: 'rodeo sports, general, light effort',
           metValue: 4,
           metCategory: 1
         },
         {
           id:77,
           name: 'rodeo sports, general, moderate effort',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:78,
           name: 'rodeo sports, general, vigorous effort',
           metValue: 7,
           metCategory: 2
         },
         {
           id:79,
           name: 'rope jumping, fast pace, 120-160 skips/min',
           metValue: 12.3,
           metCategory: 2
         },
         {
           id:80,
           name: 'rope jumping, moderate pace, 100-120 skips/min, general,  2 foot skip, plain bounce',
           metValue: 11.8,
           metCategory: 2
         },
         {
           id:81,
           name: 'rope jumping, slow pace, < 100 skips/min, 2 foot skip, rhythm bounce',
           metValue: 8.8,
           metCategory: 2
         },
         {
           id:82,
           name: 'rugby, union, team, competitive',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:83,
           name: 'rugby, touch, non-competitive',
           metValue: 6.3,
           metCategory: 2
         },
         {
           id:84,
           name: 'shuffleboard',
           metValue: 3,
           metCategory: 1
         },
         {
           id:85,
           name: 'skateboarding, general, moderate effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:86,
           name: 'skateboarding, competitive, vigorous effort',
           metValue: 6,
           metCategory: 2
         },
         {
           id:87,
           name: 'skating, roller (Taylor Code 360)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:88,
           name: 'rollerblading, in-line skating, 14.4 km/h (9.0 mph), recreational pace',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:89,
           name: 'rollerblading, in-line skating, 17.7 km/h (11.0 mph), moderate pace, exercise training',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:90,
           name: 'rollerblading, in-line skating, 21.0 to 21.7 km/h (13.0 to 13.6 mph), fast pace, exercise training',
           metValue: 12.3,
           metCategory: 2
         },
         {
           id:91,
           name: 'rollerblading, in-line skating, 24.0 km/h (15.0 mph), maximal effort',
           metValue: 14,
           metCategory: 2
         },
         {
           id:92,
           name: 'skydiving, base jumping, bungee jumping',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:93,
           name: 'soccer, competitive',
           metValue: 10,
           metCategory: 2
         },
         {
           id:94,
           name: 'soccer, casual, general (Taylor Code 540)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:95,
           name: 'softball or baseball, fast or slow pitch, general (Taylor Code 440)',
           metValue: 5,
           metCategory: 1
         },
         {
           id:96,
           name: 'softball, practice',
           metValue: 4,
           metCategory: 1
         },
         {
           id:97,
           name: 'softball, officiating',
           metValue: 4,
           metCategory: 1
         },
         {
           id:98,
           name: 'softball,pitching',
           metValue: 6,
           metCategory: 2
         },
         {
           id:99,
           name: 'sports spectator, very excited, emotional, physically moving',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:100,
           name: 'squash (Taylor Code 530)',
           metValue: 12,
           metCategory: 2
         },
         {
           id:101,
           name: 'squash, general',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:102,
           name: 'table tennis, ping pong (Taylor Code 410)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:103,
           name: 'tai chi, qi gong, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:104,
           name: 'tai chi, qi gong, sitting, light effort',
           metValue: 1.5,
           metCategory: 0
         },
         {
           id:105,
           name: 'tennis, general',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:106,
           name: 'tennis, doubles (Taylor Code 430)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:107,
           name: 'tennis, doubles',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:108,
           name: 'tennis, singles (Taylor Code 420)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:109,
           name: 'tennis, hitting balls, non-game play, moderate effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:110,
           name: 'trampoline, recreational',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:111,
           name: 'trampoline, competitive',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:112,
           name: 'volleyball (Taylor Code 400)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:113,
           name: 'volleyball, competitive, in gymnasium',
           metValue: 6,
           metCategory: 2
         },
         {
           id:114,
           name: 'volleyball, non-competitive, 6 - 9 member team, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:115,
           name: 'volleyball, beach, in sand',
           metValue: 8,
           metCategory: 2
         },
         {
           id:116,
           name: 'wrestling (one match = 5 minutes)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:117,
           name: 'wallyball, general',
           metValue: 7,
           metCategory: 2
         },
         {
           id:118,
           name: 'track and field (e.g., shot, discus, hammer throw)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:119,
           name: 'track and field (e.g., high jump, long jump, triple jump, javelin, pole vault)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:120,
           name: 'track and field (e.g., steeplechase, hurdles)',
           metValue: 10,
           metCategory: 2
         }
        ]
      },
    {
      id: 7,
      name: 'Walking',
      face: 'img/act/walking.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'backpacking (Taylor Code 050)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:1,
           name: 'backpacking, hiking or organized walking with a daypack',
           metValue: 7.8,
           metCategory: 2
         },
         {
           id:2,
           name: 'carrying 15 pound load (e.g. suitcase), level ground or downstairs',
           metValue: 5,
           metCategory: 1
         },
         {
           id:3,
           name: 'carrying 15 lb child, slow walking',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:4,
           name: 'carrying load upstairs, general',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:5,
           name: 'carrying 1 to 15 lb load, upstairs',
           metValue: 5,
           metCategory: 1
         },
         {
           id:6,
           name: 'carrying 16 to 24 lb load, upstairs',
           metValue: 6,
           metCategory: 2
         },
         {
           id:7,
           name: 'carrying 25 to 49 lb load, upstairs',
           metValue: 8,
           metCategory: 2
         },
         {
           id:8,
           name: 'carrying 50 to 74 lb load, upstairs',
           metValue: 10,
           metCategory: 2
         },
         {
           id:9,
           name: 'carrying > 74 lb load, upstairs',
           metValue: 12,
           metCategory: 2
         },
         {
           id:10,
           name: 'loading /unloading a car, implied walking',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:11,
           name: 'climbing hills, no load',
           metValue: 6.3,
           metCategory: 2
         },
         {
           id:12,
           name: 'climbing hills with 0 to 9 lb load',
           metValue: 6.5,
           metCategory: 2
         },
         {
           id:13,
           name: 'climbing hills with 10 to 20 lb load',
           metValue: 7.3,
           metCategory: 2
         },
         {
           id:14,
           name: 'climbing hills with 21 to 42 lb load',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:15,
           name: 'climbing hills with 42+ lb load',
           metValue: 9,
           metCategory: 2
         },
         {
           id:16,
           name: 'descending stairs',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:17,
           name: 'hiking, cross country (Taylor Code 040)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:18,
           name: 'hiking or walking at a normal pace through fields and hillsides',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:19,
           name: 'bird watching, slow walk',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:20,
           name: 'marching, moderate speed, military, no pack',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:21,
           name: 'marching rapidly, military, no pack',
           metValue: 8,
           metCategory: 2
         },
         {
           id:22,
           name: 'pushing or pulling stroller with child or walking with children, 2.5 to 3.1 mph',
           metValue: 4,
           metCategory: 1
         },
         {
           id:23,
           name: 'pushing a wheelchair, non-occupational',
           metValue: 3.8,
           metCategory: 1
         },
         {
           id:24,
           name: 'race walking',
           metValue: 6.5,
           metCategory: 2
         },
         {
           id:25,
           name: 'stair climbing, using or climbing up ladder (Taylor Code 030)',
           metValue: 8,
           metCategory: 2
         },
         {
           id:26,
           name: 'stair climbing, slow pace',
           metValue: 4,
           metCategory: 1
         },
         {
           id:27,
           name: 'stair climbing, fast pace',
           metValue: 8.8,
           metCategory: 2
         },
         {
           id:28,
           name: 'using crutches',
           metValue: 5,
           metCategory: 1
         },
         {
           id:29,
           name: 'walking, household',
           metValue: 2,
           metCategory: 0
         },
         {
           id:30,
           name: 'walking, less than 2.0 mph, level, strolling, very slow',
           metValue: 2,
           metCategory: 0
         },
         {
           id:31,
           name: 'walking, 2.0 mph, level, slow pace, firm surface',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:32,
           name: 'walking for pleasure (Taylor Code 010)',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:33,
           name: 'walking from house to car or bus, from car or bus to go places, from car or bus to and from the worksite',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:34,
           name: 'walking to neighborÆs house or familyÆs house for social reasons',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:35,
           name: 'walking the dog',
           metValue: 3,
           metCategory: 1
         },
         {
           id:36,
           name: 'walking, 2.5 mph, level, firm surface',
           metValue: 3,
           metCategory: 1
         },
         {
           id:37,
           name: 'walking, 2.5 mph, downhill',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:38,
           name: 'walking, 2.8 to 3.2 mph, level, moderate pace, firm surface',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:39,
           name: 'walking, 3.5 mph, level, brisk, firm surface, walking for exercise',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:40,
           name: 'walking, 2.9 to 3.5 mph, uphill, 1 to 5% grade',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:41,
           name: 'walking, 2.9 to 3.5 mph, uphill, 6% to 15% grade',
           metValue: 8,
           metCategory: 2
         },
         {
           id:42,
           name: 'walking, 4.0 mph, level, firm surface, very brisk pace',
           metValue: 5,
           metCategory: 1
         },
         {
           id:43,
           name: 'walking, 4.5 mph, level, firm surface, very, very brisk',
           metValue: 7,
           metCategory: 2
         },
         {
           id:44,
           name: 'walking, 5.0 mph, level, firm surface',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:45,
           name: 'walking, 5.0 mph, uphill, 3% grade',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:46,
           name: 'walking, for pleasure, work break',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:47,
           name: 'walking, grass track',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:48,
           name: 'walking, normal pace, plowed field or sand',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:49,
           name: 'walking, to work or class (Taylor Code 015)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:50,
           name: 'walking, to and from an outhouse',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:51,
           name: 'walking, for exercise, 3.5 to 4 mph, with ski poles, Nordic walking, level, moderate pace',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:52,
           name: 'walking, for exercise, 5.0 mph, with ski poles, Nordic walking, level, fast pace',
           metValue: 9.5,
           metCategory: 2
         },
         {
           id:53,
           name: 'walking, for exercise, with ski poles, Nordic walking, uphill',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:54,
           name: 'walking, backwards, 3.5 mph, level',
           metValue: 6,
           metCategory: 2
         },
         {
           id:55,
           name: 'walking, backwards, 3.5 mph, uphill, 5% grade',
           metValue: 8,
           metCategory: 2
         }
        ]
      },
    {
      id: 8,
      name: 'Water activities',
      face: 'img/act/water_activities.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'boating, power, driving',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:1,
           name: 'boating, power, passenger, light',
           metValue: 1.3,
           metCategory: 0
         },
         {
           id:2,
           name: 'canoeing, on camping trip (Taylor Code 270)',
           metValue: 4,
           metCategory: 1
         },
         {
           id:3,
           name: 'canoeing, harvesting wild rice, knocking rice off the stalks',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:4,
           name: 'canoeing, portaging',
           metValue: 7,
           metCategory: 2
         },
         {
           id:5,
           name: 'canoeing, rowing, 2.0-3.9 mph, light effort',
           metValue: 2.8,
           metCategory: 0
         },
         {
           id:6,
           name: 'canoeing, rowing, 4.0-5.9 mph, moderate effort',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:7,
           name: 'canoeing, rowing, kayaking, competition, >6 mph, vigorous effort',
           metValue: 12.5,
           metCategory: 2
         },
         {
           id:8,
           name: 'canoeing, rowing, for pleasure, general (Taylor Code 250)',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:9,
           name: 'canoeing, rowing, in competition, or crew or sculling (Taylor Code 260)',
           metValue: 12,
           metCategory: 2
         },
         {
           id:10,
           name: 'diving, springboard or platform',
           metValue: 3,
           metCategory: 1
         },
         {
           id:11,
           name: 'kayaking, moderate effort',
           metValue: 5,
           metCategory: 1
         },
         {
           id:12,
           name: 'paddle boat',
           metValue: 4,
           metCategory: 1
         },
         {
           id:13,
           name: 'sailing, boat and board sailing, windsurfing, ice sailing, general (Taylor Code 235)',
           metValue: 3,
           metCategory: 1
         },
         {
           id:14,
           name: 'sailing, in competition',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:15,
           name: 'sailing, Sunfish/Laser/Hobby Cat, Keel boats, ocean sailing, yachting, leisure',
           metValue: 3.3,
           metCategory: 1
         },
         {
           id:16,
           name: 'skiing, water or wakeboarding (Taylor Code 220)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:17,
           name: 'jet skiing, driving, in water',
           metValue: 7,
           metCategory: 2
         },
         {
           id:18,
           name: 'skindiving, fast',
           metValue: 15.8,
           metCategory: 2
         },
         {
           id:19,
           name: 'skindiving, moderate',
           metValue: 11.8,
           metCategory: 2
         },
         {
           id:20,
           name: 'skindiving, scuba diving, general (Taylor Code 310)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:21,
           name: 'snorkeling (Taylor Code 310)',
           metValue: 5,
           metCategory: 1
         },
         {
           id:22,
           name: 'surfing, body or board, general',
           metValue: 3,
           metCategory: 1
         },
         {
           id:23,
           name: 'surfing, body or board, competitive',
           metValue: 5,
           metCategory: 1
         },
         {
           id:24,
           name: 'paddle boarding, standing',
           metValue: 6,
           metCategory: 2
         },
         {
           id:25,
           name: 'swimming laps, freestyle, fast, vigorous effort',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:26,
           name: 'swimming laps, freestyle, front crawl, slow, light or moderate effort',
           metValue: 5.8,
           metCategory: 1
         },
         {
           id:27,
           name: 'swimming, backstroke, general, training or competition',
           metValue: 9.5,
           metCategory: 2
         },
         {
           id:28,
           name: 'swimming, backstroke, recreational',
           metValue: 4.8,
           metCategory: 1
         },
         {
           id:29,
           name: 'swimming, breaststroke, general, training or competition',
           metValue: 10.3,
           metCategory: 2
         },
         {
           id:30,
           name: 'swimming, breaststroke, recreational',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:31,
           name: 'swimming, butterfly, general',
           metValue: 13.8,
           metCategory: 2
         },
         {
           id:32,
           name: 'swimming, crawl, fast speed, ~75 yards/minute, vigorous effort',
           metValue: 10,
           metCategory: 2
         },
         {
           id:33,
           name: 'swimming, crawl, medium speed, ~50 yards/minute, vigorous effort',
           metValue: 8.3,
           metCategory: 2
         },
         {
           id:34,
           name: 'swimming, lake, ocean, river (Taylor Codes 280, 295)',
           metValue: 6,
           metCategory: 2
         },
         {
           id:35,
           name: 'swimming, leisurely, not lap swimming, general',
           metValue: 6,
           metCategory: 2
         },
         {
           id:36,
           name: 'swimming, sidestroke, general',
           metValue: 7,
           metCategory: 2
         },
         {
           id:37,
           name: 'swimming, synchronized',
           metValue: 8,
           metCategory: 2
         },
         {
           id:38,
           name: 'swimming, treading water, fast, vigorous effort',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:39,
           name: 'swimming, treading water, moderate effort, general',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:40,
           name: 'tubing, floating on a river, general',
           metValue: 2.3,
           metCategory: 0
         },
         {
           id:41,
           name: 'water aerobics, water calisthenics',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:42,
           name: 'water polo',
           metValue: 10,
           metCategory: 2
         },
         {
           id:43,
           name: 'water volleyball',
           metValue: 3,
           metCategory: 1
         },
         {
           id:44,
           name: 'water jogging',
           metValue: 9.8,
           metCategory: 2
         },
         {
           id:45,
           name: 'water walking, light effort, slow pace',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:46,
           name: 'water walking, moderate effort, moderate pace',
           metValue: 4.5,
           metCategory: 1
         },
         {
           id:47,
           name: 'water walking, vigorous effort, brisk pace',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:48,
           name: 'whitewater rafting, kayaking, or canoeing',
           metValue: 5,
           metCategory: 1
         },
         {
           id:49,
           name: 'windsurfing, not pumping for speed',
           metValue: 5,
           metCategory: 1
         },
         {
           id:50,
           name: 'windsurfing or kitesurfing, crossing trial',
           metValue: 11,
           metCategory: 2
         },
         {
           id:51,
           name: 'windsurfing, competition, pumping for speed',
           metValue: 13.5,
           metCategory: 2
         }
        ]
      },
    {
      id: 9,
      name: 'Winter activities',
      face: 'img/act/winter_activities.png',
      subtitle: 'Subtitle0',
      specific: [
         {
           id:0,
           name: 'dog sledding, mushing',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:1,
           name: 'dog sledding, passenger',
           metValue: 2.5,
           metCategory: 0
         },
         {
           id:2,
           name: 'moving ice house, set up/drill holes',
           metValue: 6,
           metCategory: 2
         },
         {
           id:3,
           name: 'ice fishing, sitting',
           metValue: 2,
           metCategory: 0
         },
         {
           id:4,
           name: 'skating, ice dancing',
           metValue: 14,
           metCategory: 2
         },
         {
           id:5,
           name: 'skating, ice, 9 mph or less',
           metValue: 5.5,
           metCategory: 1
         },
         {
           id:6,
           name: 'skating, ice, general (Taylor Code 360)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:7,
           name: 'skating, ice, rapidly, more than 9 mph, not competitive',
           metValue: 9,
           metCategory: 2
         },
         {
           id:8,
           name: 'skating, speed, competitive',
           metValue: 13.3,
           metCategory: 2
         },
         {
           id:9,
           name: 'ski jumping, climb up carrying skis',
           metValue: 7,
           metCategory: 2
         },
         {
           id:10,
           name: 'skiing, general',
           metValue: 7,
           metCategory: 2
         },
         {
           id:11,
           name: 'skiing, cross country, 2.5 mph, slow or light effort, ski walking',
           metValue: 6.8,
           metCategory: 2
         },
         {
           id:12,
           name: 'skiing, cross country, 4.0-4.9 mph, moderate speed and effort, general',
           metValue: 9,
           metCategory: 2
         },
         {
           id:13,
           name: 'skiing, cross country, 5.0-7.9 mph, brisk speed, vigorous effort',
           metValue: 12.5,
           metCategory: 2
         },
         {
           id:14,
           name: 'skiing, cross country, >8.0 mph, elite skier, racing',
           metValue: 15,
           metCategory: 2
         },
         {
           id:15,
           name: 'skiing, cross country, hard snow, uphill, maximum, snow mountaineering',
           metValue: 15.5,
           metCategory: 2
         },
         {
           id:16,
           name: 'skiing, cross-country, skating',
           metValue: 13.3,
           metCategory: 2
         },
         {
           id:17,
           name: 'skiing, cross-country, biathlon, skating technique',
           metValue: 13.5,
           metCategory: 2
         },
         {
           id:18,
           name: 'skiing, downhill, alpine or snowboarding, light effort, active time only',
           metValue: 4.3,
           metCategory: 1
         },
         {
           id:19,
           name: 'skiing, downhill, alpine or snowboarding, moderate effort, general, active time only',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:20,
           name: 'skiing, downhill, vigorous effort, racing',
           metValue: 8,
           metCategory: 2
         },
         {
           id:21,
           name: 'skiing, roller, elite racers',
           metValue: 12.5,
           metCategory: 2
         },
         {
           id:22,
           name: 'sledding, tobogganing, bobsledding, luge (Taylor Code 370)',
           metValue: 7,
           metCategory: 2
         },
         {
           id:23,
           name: 'snow shoeing, moderate effort',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:24,
           name: 'snow shoeing, vigorous effort',
           metValue: 10,
           metCategory: 2
         },
         {
           id:25,
           name: 'snowmobiling, driving, moderate',
           metValue: 3.5,
           metCategory: 1
         },
         {
           id:26,
           name: 'snowmobiling, passenger',
           metValue: 2,
           metCategory: 0
         },
         {
           id:27,
           name: 'snow shoveling, by hand, moderate effort',
           metValue: 5.3,
           metCategory: 1
         },
         {
           id:28,
           name: 'snow shoveling, by hand, vigorous effort',
           metValue: 7.5,
           metCategory: 2
         },
         {
           id:29,
           name: 'snow blower, walking and pushing',
           metValue: 2.5,
           metCategory: 0
          }
        ]
      }
    ];

  return {
    all: function() {
      return activities;
    },
    get: function(activityId) {
      for (var i = 0; i < activities.length; i++) {
        if (activities[i].id === parseInt(activityId)) {
          return activities[i];
        }
      }
      return null;
    }
  };
})

.factory('MetValues', function() {

  var values = [
    {
      id: 0,
      name: 'light',
      no2:  [1.899E16, 2.49E16, 1.00E29],
      ch2o: [1.323E19, 1.569E19, 1.00E29],
      so2:  [2.212E16, 2.518E16, 1.00E29]
    },
    {
      id: 1,
      name: 'moderate',
      no2:  [1.0312E16, 1.89949E16, 1.00E29],
      ch2o: [1.482E18, 1.323E19, 1.00E29],
      so2:  [1.113E16, 2.212E16, 1.00E29]
    },
    {
      id: 2,
      name: 'vigorous',
      no2:  [5.9429E15, 1.89949E16, 1.00E29],
      ch2o: [7.12626E17, 1.32309E19, 1.00E29],
      so2:  [7.565E15, 2.212E16, 1.00E29]
    }
  ];

  return values;

})

.factory('Locations', function() {
  // Might use a resource here that returns a JSON array

  var places = [
    {
      id: 0,
      name: 'Palermo - Italy',
      flag: 'img/flags/ita.png',
      latitude: 38.13729,
      longitude: 13.34728
    },
    {
      id: 1,
      name: 'Catania - Italy',
      flag: 'img/flags/ita.png',
      latitude: 37.46041,
      longitude: 15.03088
    },
    {
      id: 2,
      name: 'Rome - Italy',
      flag: 'img/flags/ita.png',
      latitude: 41.89880,
      longitude: 12.54513
    },
    {
      id: 3,
      name: 'Milan - Italy',
      flag: 'img/flags/ita.png',
      latitude: 45.46134,
      longitude: 9.15950
    },
    {
      id: 4,
      name: 'Berlino - Germany',
      flag: 'img/flags/ger.png',
      latitude: 52.51733,
      longitude: 13.38886
    },
    {
      id: 5,
      name: 'Frankfurt - Germany',
      flag: 'img/flags/ger.png',
      latitude: 50.11092,
      longitude: 8.68209
    },
    {
      id: 6,
      name: 'Monaco - Germany',
      flag: 'img/flags/ger.png',
      latitude: 48.15500,
      longitude: 11.54183
    },
    {
      id: 7,
      name: 'Struttgart - Germany',
      flag: 'img/flags/ger.png',
      latitude: 48.77928,
      longitude: 9.17721
    },
    {
      id: 8,
      name: 'London - United Kingdom',
      flag: 'img/flags/grb.png',
      latitude: 51.48977,
      longitude: -0.08818
    },
    {
      id: 9,
      name: 'Manchester - United Kingdom',
      flag: 'img/flags/grb.png',
      latitude: 53.44249,
      longitude: -2.23336
    },
    {
      id: 10,
      name: 'Paris - France',
      flag: 'img/flags/fra.png',
      latitude: 48.85888,
      longitude: 2.3469
    },
    {
      id: 11,
      name: 'Nantes - France',
      flag: 'img/flags/fra.png',
      latitude:  47.23825,
      longitude: -1.56033
    },
    {
      id: 12,
      name: 'Madrid - Spain',
      flag: 'img/flags/spa.png',
      latitude: 40.47806,
      longitude: -3.70343
    },
    {
      id: 13,
      name: 'Malaga - Spain',
      flag: 'img/flags/spa.png',
      latitude: 36.76441,
      longitude: -4.42419
    },
    {
      id: 14,
      name: 'Athens - Greece',
      flag: 'img/flags/gre.png',
      latitude: 37.98432,
      longitude: 23.72798
    },
    {
      id: 15,
      name: 'Varsavia - Poland',
      flag: 'img/flags/pol.png',
      latitude: 52.23321,
      longitude: 21.06142
    },
    {
      id: 16,
      name: 'Helsinky - Finland',
      flag: 'img/flags/fin.png',
      latitude: 60.11070,
      longitude: 25.01866
    }
  ];
  return {
    all: function() {
      return places;
    },
    get: function(placeId) {
      for (var j = 0; j < places.length; j++) {
        if (places[j].id === parseInt(placeId)) {
          return places[j];
        }
      }
      return null;
    }
  };

})

;
