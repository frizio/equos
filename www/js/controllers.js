angular.module('starter.controllers', [])

  .controller('HomeCtrl', function($scope, $rootScope, $state,
                          $cordovaGeolocation, $ionicLoading, $ionicPopup,
                          $http, SERVER, MetValues) {

    $scope.showAQcard = false;

    $scope.badColor = 'white';
    $scope.modestColor = 'white';
    $scope.goodColor = 'white';

    $scope.disableAQIbutton = !($rootScope.currentLatitude && $rootScope.currentLongitude &&
                                $rootScope.activity && $rootScope.subactivity);

    ionic.Platform.ready(function() {
      //$scope.acquirePosition();
    });

    $scope.quit = function() {
      //ionic.Platform.exitApp();
      navigator.app.exitApp();
    };

    $scope.reset = function() {

      $scope.showAQcard = false;
      $rootScope.activity = false;
      $rootScope.subactivity = false;

      $rootScope.currentPlace = "";
      $rootScope.currentLatitude = false;
      $rootScope.currentLongitude = false;

      $scope.no2 = 0;
      $scope.ch2o = 0;
      $scope.so2 = 0;

    };

    $scope.evaluateAQI = function() {

      var currentActivityCategory = $rootScope.subactivity.metCategory;

      var no2Quality = 10;
      var currentNo2value = $scope.no2;
      var no2Thresholds = MetValues[currentActivityCategory].no2;

      if (currentNo2value > 0 && currentNo2value < no2Thresholds[0]) {
        no2Quality = 2;
      } else if (currentNo2value > no2Thresholds[0] && currentNo2value < no2Thresholds[1]) {
        no2Quality = 1;
      } else if (currentNo2value > no2Thresholds[1] && currentNo2value < no2Thresholds[2]) {
        no2Quality = 0;
      } else  {
        no2Quality = 3;
      };

      var ch2oQuality = 10;
      var currentCh2oValue = $scope.ch2o;
      var  ch2oThresholds = MetValues[currentActivityCategory].ch2o;

      if (currentCh2oValue > 0 && currentCh2oValue < ch2oThresholds[0]) {
        ch2oQuality = 2;
      } else if (currentCh2oValue > ch2oThresholds[0] && currentCh2oValue < ch2oThresholds[1]) {
        ch2oQuality = 1;
      } else if (currentCh2oValue > ch2oThresholds[1] && currentCh2oValue < ch2oThresholds[2]) {
        ch2oQuality = 0;
      } else  {
        ch2oQuality = 3;
      };

      var so2Quality = 10;
      var currentSo2value = $scope.so2;
      var  so2Thresholds = MetValues[currentActivityCategory].so2;

      if (currentSo2value > 0 && currentSo2value < so2Thresholds[0]) {
        so2Quality = 2;
      } else if (currentSo2value > so2Thresholds[0] && currentSo2value < so2Thresholds[1]) {
        so2Quality = 1;
      } else if (currentSo2value > so2Thresholds[1] && currentSo2value < so2Thresholds[2]) {
        so2Quality = 0;
      } else  {
        so2Quality = 3;
      };

      var aqi = Math.min(so2Quality, ch2oQuality, no2Quality);

      switch (aqi) {
        case 0:
          $scope.badColor = '#ff0000';
          $scope.modestColor = 'white';
          $scope.goodColor = 'white';
          break;
        case 1:
          $scope.badColor = 'white';
          $scope.modestColor = '#ffff00';
          $scope.goodColor = 'white';
          break;
        case 2:
          $scope.badColor = 'white';
          $scope.modestColor = 'white';
          $scope.goodColor = '#40ff00';
          break;
        case 3:
          $scope.badColor = 'grey';
          $scope.modestColor = 'grey';
          $scope.goodColor = 'grey';
          break;
      }

      $scope.no2Quality = no2Quality;
      $scope.ch2oQuality = ch2oQuality;
      $scope.so2Quality = so2Quality;

    }

    $scope.submit = function() {

      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Sending request to Server'
      });

      $http.get(SERVER.url,
                { params: { "latitude": $rootScope.currentLatitude,
                            "longitude": $rootScope.currentLongitude } })
            .success(function(data) {
                //$scope.currentData = data;
                $scope.no2 =  data.no2;
                $scope.ch2o = data.ch2o;
                $scope.so2 = data.so2;
                $ionicLoading.hide();
                //$ionicPopup.hide();

                $scope.evaluateAQI();
                $scope.showAQcard = true;
            })
            .error(function(data) {
                $ionicLoading.hide();
                alert("Connection server error!");
            });

    };

    $scope.acquirePosition = function() {

      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
      });

      var posOptions = {
        timeout: 10000,
        enableHighAccuracy: false
      };

      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
         var lat  = position.coords.latitude;
         var long = position.coords.longitude;
         //console.log(lat + '   ' + long);
         //$rootScope.currentPlace = "Current Position";
         $rootScope.currentLatitude = lat;
         $rootScope.currentLongitude = long;
         $ionicLoading.hide();

         $http.get(SERVER.nominatim,
                   { params: { "email": 'email@gmail.com',
                               "format": 'json',
                               "lat": lat,
                               "lon": long } })
               .success(function(data) {
                   $rootScope.currentPlace =  data.display_name;
               })
               .error(function(data) {
                   $rootScope.currentPlace = "Current Position";
               });

      }, function(err) {
         //console.log(err);
         $ionicLoading.hide();
         $ionicPopup.alert({
           title: 'Location error',
           content: 'The Gps sensor seems disabled.\
                     Check Device Settings e retry to acquire the position!'
         }).then(function(res) {
           //console.log('Location error ');
         });
      });

    };

    $scope.goToLocations = function() {
      $scope.showAQcard = false;
      $state.go('tab.locations');
    };

    $scope.goToActivities = function() {
      $scope.showAQcard = false;
      $state.go('tab.activities');
    };

    $scope.aqiDetails = function() {

      $ionicPopup.alert({
        scope: $scope,
        title: 'Air Quality Details',
        content: '<h5>Values (mol/cm2)</h5><b>NO<sub>2</sub></b> : {{no2.toExponential()}} </p> <p><b>CH<sub>2</sub>O</b> : {{ch2o.toExponential()}}</p> <p><b>SO<sub>2</sub></b> : {{so2.toExponential()}}</p>'
      }).then(function(res) {
        //console.log('Location error ');
         $ionicPopup.hide();

      });
    };

    $scope.metMessage = function() {
      $ionicPopup.alert({
        scope: $scope,
        title: 'MET',
        content: '<p align="justify">One metabolic equivalent of task (MET) is defined as the amount of oxygen consumed while sitting at rest and is equal to 3.5 ml O<sub>2</sub> per kg body weight pre min (ml/kg/min). The MET concept represents a simple, practical, and easily understood procedure for expressing the energy cost of physical activities as a multiple of the resting metabolic rate.</p>'
      }).then(function(res) {
        //console.log('Location error ');
         $ionicPopup.hide();

      });
    };


  })

  .controller('LocationsCtrl', function($scope, $rootScope, SERVER, $http, $state, $ionicLoading, Locations) {

    $scope.locations = Locations.all();

    $scope.add = function(place) {
      //$rootScope.currentPlace = place.name;
      $rootScope.currentLatitude = place.latitude;
      $rootScope.currentLongitude = place.longitude;

      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Reverse Geodecoding'
      });

      $http.get(SERVER.nominatim,
                { params: { "email" : 'email@gmail.com',
                            "format": 'json',
                            "lat": place.latitude,
                            "lon": place.longitude } })
            .success(function(data) {
                $ionicLoading.hide();
                $rootScope.currentPlace =  data.display_name;
            })
            .error(function(data) {
                $ionicLoading.hide();
                alert("Nominatim error!");
            });


      $state.go('tab.home');
    }

  })

  .controller('ActivitiesCtrl', function($scope, $rootScope, Activities, Locations) {

    $scope.activities = Activities.all();

    $scope.add = function(act) {
      $rootScope.activity = act;
    }

  })

  .controller('ActivitiesDetailCtrl', function($scope, $state, $stateParams, $rootScope, Activities) {

    $scope.activity = Activities.get($stateParams.activityId);

    $scope.add = function(subactivity) {
      $rootScope.subactivity = subactivity;
      $state.go('tab.home');
    }

  })

  .controller('CreditsCtrl', function($scope) {

  })

  .controller('PageController', function($scope, $location,
                                         $anchorScroll, $ionicScrollDelegate) {

    $scope.scrollToAnchorWithinCurrentPage = function(anchor) {
    $location.hash(anchor);
    var handle = $ionicScrollDelegate.$getByHandle('content');
    handle.anchorScroll();
    };

  })


;
