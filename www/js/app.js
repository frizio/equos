angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('top');
  $ionicConfigProvider.views.transition('android')
  $ionicConfigProvider.views.maxCache(1);
  $ionicConfigProvider.navBar.alignTitle('center');

  $stateProvider
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/tab-home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('tab.locations', {
    url: '/locations',
    views: {
      'tab-locations': {
        templateUrl: 'templates/tab-locations.html',
        controller: 'LocationsCtrl'
      }
    }
  })


  .state('tab.activities', {
      url: '/activities',
        views: {
        'tab-activities': {
          templateUrl: 'templates/tab-activities.html',
          controller: 'ActivitiesCtrl'
        }
      }
    })
    .state('tab.activities-detail', {
      url: '/activities/:activityId',
      views: {
        'tab-activities': {
          templateUrl: 'templates/activities-detail.html',
          controller: 'ActivitiesDetailCtrl'
        }
      }
    })

  .state('tab.credits', {
    url: '/credits',
    views: {
      'tab-credits': {
        templateUrl: 'templates/tab-credits.html',
        controller: 'CreditsCtrl'
      }
    }
  })

  .state('tab.acknowledgement', {
    url: '/credits/acknowledgement',
    views: {
      'tab-credits': {
      templateUrl: 'templates/acknowledgement.html'
      }
    }
  })

  .state('tab.informations', {
    url: '/credits/informations',
    views: {
      'tab-credits': {
      templateUrl: 'templates/informations.html',

      }
    }
  })

  .state('tab.contact', {
    url: '/credits/contact',
    views: {
      'tab-credits': {
      templateUrl: 'templates/contact.html'
      }
    }
  })

  ;

  $urlRouterProvider.otherwise('/tab/home');

})

.constant('SERVER', {
  // if using local server
  //url: 'http://localhost:3000'

  // In production using Cloud Server
  url: 'http://80.88.90.177/eQUOSwebService/rest/pollutions/getby',
  nominatim: 'http://nominatim.openstreetmap.org/reverse'
});

;
