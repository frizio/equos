# eQUOS mobile App
eQUOS (Environment QUality for Outdoor Sports) suggests to the final user whether to perform a specific <a>outdoor physical activity</a> in a specific geographical area on the basis of the air quality. In particular, eQUOS app considers and processes datasets providing near real-time data about: <a>Nitrogen dioxide</a>, <a>Formaldehyde</a> and <a>Sulphur dioxide</a>.

The app is published under the European Union Public License (EUPL) Version 1.1. See LICENSE file.

# Public Service Data Availability
http://80.88.90.177/eQUOSwebService/rest/pollutions/getby?latitude=38.123456&longitude=18.123456

Change latitude and langitude values with the user values to obtain different pollution values. The datasets are valid only for the Europe-Zone.

# App Stores
The App is not available on stores at this time. Use the .apk available in the download section to try the app.

# App Development
The app has been build based on the Ionic Framework as hybrid app.
Using the Ionic and the Apache Cordova frameworks this app has been exported to native apps for Android and iOS.

# Contacts
Euro-Mediterranean Institute of Science and Technology - Italy

Filippo Macaluso, PhD;

E-mail: fil.macaluso@gmail.com


Giosue' Lo Bosco, PhD;

E-mail: giosue.lobosco@gmail.com


Maurizio La Rocca, BS;

E-mail: mrz.larocca@gmail.com


# Funding
This application has been developed within the MyGEOSS project, which has received funding from the European Union’s Horizon 2020 research and innovation programme.

# Disclaimer
The JRC, or as the case may be the European Commission, shall not be held liable for any direct or indirect, incidental, consequential or other damages, including but not limited to the loss of data, loss of profits, or any other financial loss arising from the use of this application, or inability to use it, even if the JRC is notified of the possibility of such damages.
